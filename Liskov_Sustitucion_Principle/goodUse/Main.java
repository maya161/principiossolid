public class Main {
  public static void main(String[] args) {
    ICar car;
    String cardType = args[0];
    if ("car" == cardType) {
      car = new Car();
    } else if ("electric" == cardType) {
      car = new ElectricCar();
    } else {
      throw new RuntimeException("Invalid car");
    }
  }
}
