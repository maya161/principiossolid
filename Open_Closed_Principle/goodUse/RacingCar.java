public class RacingCar extends Car {

  @Override
  void accelerate() {
    injectExtraGas();
    super.accelerate();
  }

  private void injectExtraGas() {
    // do..
  }

}
