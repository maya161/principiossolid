public class ElectricCar implements ICar {

  private int battery;

  @Autowired
  void accelerating() {
    if (hasBattery()) {
      System.out.println("accelerating the car");
    } else {
      System.out.println("I can not accelerate the car");
    }
  }

  @Autowired
  void stop() {
    System.out.println("stop the car");
  }

  public boolean hasBattery() {
    System.out.println("checking battery");
    if (battery < 95) {
      System.out.println("the battery is very low :(");
      return false;
    } else {
      System.out.println("battery OK :)");
      return true;
    }
  }
}
