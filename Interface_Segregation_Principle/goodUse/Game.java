public class Game implements IProducto, IRestrictedProduct {

  private int age;

  @Override
  public String getType() {
    return "shoes";
  }

  @Override
  public int getAge() {
    return age;
  }

}
